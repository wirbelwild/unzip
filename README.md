[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/unzip)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/unzip/v/stable)](https://packagist.org/packages/bitandblack/unzip)
[![Total Downloads](https://poser.pugx.org/bitandblack/unzip/downloads)](https://packagist.org/packages/bitandblack/unzip)
[![License](https://poser.pugx.org/bitandblack/unzip/license)](https://packagist.org/packages/bitandblack/unzip)

# Bit&Black Unzip

Unpacks ZIP content on the fly, without writing to the file system. Handles files, strings and ressources.

## Installation

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/unzip). Add it to your project by running `$ composer require bitandblack/unzip`.

## Usage

### Using the auto ZIP handler

Maybe the easiest way is to use the `AutoZIPHandler`. It detects the kind of ZIP content by its own and returns a `FileType`, `StringType` or `StreamType` object.

````php
<?php 

use BitAndBlack\Unzip\AutoZIPHandler;
 
$zipContent = AutoZIPHandler::create($whateverZIPContent);
````

Call `getContents()` to see what is inside the ZIP.

The ZIP content can be extracted to the file system by calling `extractTo()`.

### Manual set up

You can set up the file type by your own too. For example:

````php
<?php 

use BitAndBlack\Unzip\Type\FileType;
 
$fileType = new FileType('/path/to/file.zip');

$contents = $fileType->getContents();
````

## Help

If you have questions feel free to contact us under `hello@bitandblack.com`.