<?php

/**
 * Bit&Black Unzip. Unpacks ZIP files on the fly.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Unzip\Tests;

use BitAndBlack\Helpers\FileSystemHelper;
use BitAndBlack\Unzip\AutoZIPHandler;
use BitAndBlack\Unzip\Type\FileType;
use BitAndBlack\Unzip\Type\StreamType;
use BitAndBlack\Unzip\Type\StringType;
use PHPUnit\Framework\TestCase;

/**
 * Class AutoZIPHandlerTest. 
 * 
 * @package BitAndBlack\Unzip\Tests
 */
class AutoZIPHandlerTest extends TestCase
{
    /**
     * @var \BitAndBlack\Unzip\Tests\Helper
     */
    private static Helper $helper;

    public static function setUpBeforeClass(): void
    {
        self::$helper = new Helper();
    }
    
    /**
     * Removes all test files at the end.
     */
    public static function tearDownAfterClass(): void
    {
        if (file_exists(self::$helper->getZIPFile())) {
            unlink(self::$helper->getZIPFile());
        }

        if (file_exists(self::$helper->getFolderName())) {
            FileSystemHelper::deleteFolder(self::$helper->getFolderName());
        }
    }

    /**
     * @throws \BitAndBlack\Unzip\Exception\CouldNotHandleFileTypeException
     * @throws \BitAndBlack\Unzip\Exception\CouldNotReadFileException
     * @throws \PhpZip\Exception\ZipException
     */
    public function testCanHandleDifferentTypes(): void 
    {
        $file = self::$helper->getZIPFile();

        self::assertInstanceOf(
            FileType::class,
            AutoZIPHandler::create($file)
        );

        $stream = self::$helper->getZIPStream();

        self::assertInstanceOf(
            StreamType::class,
            AutoZIPHandler::create($stream)
        );

        $string = self::$helper->getZIPString();

        self::assertInstanceOf(
            StringType::class,
            AutoZIPHandler::create($string)
        );
    }
}
