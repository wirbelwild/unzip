<?php

/**
 * Bit&Black Unzip. Unpacks ZIP files on the fly.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Unzip\Tests;

use BitAndBlack\Unzip\Exception;
use ZipStream\Option\Archive;
use ZipStream\ZipStream;

/**
 * Class Helper.
 * 
 * @package BitAndBlack\Unzip\Tests
 */
class Helper
{
    /**
     * @var resource 
     */
    private $zipStream;

    /**
     * @var string 
     */
    private string $zipString;

    /**
     * @var string 
     */
    private string $zipFile = __DIR__.DIRECTORY_SEPARATOR.'Archive.zip';

    /**
     * @var string 
     */
    private string $folderName = __DIR__.DIRECTORY_SEPARATOR.'Archive';

    /**
     * Helper constructor.
     *
     * @throws \BitAndBlack\Unzip\Exception
     * @throws \ZipStream\Exception\OverflowException
     */
    public function __construct()
    {
        $zipStream = fopen('php://memory', 'wb+');

        if (false === $zipStream) {
            throw new Exception('Cannot use "php://memory".');
        }

        $this->zipStream = $zipStream;
        
        $zipStreamOptions = new Archive();
        $zipStreamOptions->setOutputStream($this->zipStream);

        $zipStreamHandler = new ZipStream(null, $zipStreamOptions);
        $zipStreamHandler->addFile('file1.txt', 'Hello World 1!');
        $zipStreamHandler->addFile('file2.txt', 'Hello World 2!');

        $zipStreamHandler->finish();

        rewind($this->zipStream);
        $zipString = (string) stream_get_contents($this->zipStream);

        $this->zipString = $zipString;
        
        file_put_contents(
            $this->zipFile,
            $zipString
        );
    }
    
    public function __destruct()
    {
        if (is_resource($this->zipStream)) {
            fclose($this->zipStream);
        }
    }

    /**
     * @return string
     */
    public function getZIPFile(): string 
    {
        return $this->zipFile;
    }

    /**
     * @return resource
     */
    public function getZIPStream()
    {
        return $this->zipStream;
    }

    /**
     * @return string
     */
    public function getZIPString(): string 
    {
        return $this->zipString;
    }

    /**
     * @return string
     */
    public function getFolderName(): string
    {
        return $this->folderName;
    }
}