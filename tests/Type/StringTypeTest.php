<?php

/**
 * Bit&Black Unzip. Unpacks ZIP files on the fly.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Unzip\Tests\Type;

use BitAndBlack\Helpers\FileSystemHelper;
use BitAndBlack\Unzip\Tests\Helper;
use BitAndBlack\Unzip\Type\StringType;
use PHPUnit\Framework\TestCase;

/**
 * Class StringTypeTest. 
 * 
 * @package BitAndBlack\Unzip\Tests
 */
class StringTypeTest extends TestCase
{
    /**
     * @var \BitAndBlack\Unzip\Tests\Helper
     */
    private static Helper $helper;

    public static function setUpBeforeClass(): void
    {
        self::$helper = new Helper();
    }
    
    /**
     * Removes all test files at the end.
     */
    public static function tearDownAfterClass(): void
    {
        if (file_exists(self::$helper->getZIPFile())) {
            unlink(self::$helper->getZIPFile());
        }

        if (file_exists(self::$helper->getFolderName())) {
            FileSystemHelper::deleteFolder(self::$helper->getFolderName());
        }
    }

    /**
     * @throws \PhpZip\Exception\ZipException
     */
    public function testCanUnzip(): void
    {
        $zipString = self::$helper->getZIPString();
        
        self::assertSame(
            'string',
            gettype($zipString)
        );
        
        $stringType = new StringType($zipString);

        $contents = $stringType->getContents();
        
        self::assertCount(
            2,
            $contents
        );

        $keys = array_keys($contents);
        $values = array_values($contents);

        self::assertSame(
            'file1.txt',
            $keys[0]
        );

        self::assertSame(
            'Hello World 1!',
            $values[0]
        );

        self::assertSame(
            'file2.txt',
            $keys[1]
        );

        self::assertSame(
            'Hello World 2!',
            $values[1]
        );
    }

}
