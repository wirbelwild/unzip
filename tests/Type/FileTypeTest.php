<?php

/**
 * Bit&Black Unzip. Unpacks ZIP files on the fly.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Unzip\Tests\Type;

use BitAndBlack\Helpers\FileSystemHelper;
use BitAndBlack\Unzip\Exception\CouldNotReadFileException;
use BitAndBlack\Unzip\Tests\Helper;
use BitAndBlack\Unzip\Type\FileType;
use PHPUnit\Framework\TestCase;

/**
 * Class FileTypeTest. 
 * 
 * @package BitAndBlack\Unzip\Tests
 */
class FileTypeTest extends TestCase
{
    /**
     * @var \BitAndBlack\Unzip\Tests\Helper
     */
    private static Helper $helper;

    public static function setUpBeforeClass(): void
    {
        self::$helper = new Helper();
    }
    
    /**
     * Removes all test files at the end.
     */
    public static function tearDownAfterClass(): void
    {
        if (file_exists(self::$helper->getZIPFile())) {
            unlink(self::$helper->getZIPFile());
        }
        
        if (file_exists(self::$helper->getFolderName())) {
            FileSystemHelper::deleteFolder(self::$helper->getFolderName());
        }
    }

    /**
     * @throws \BitAndBlack\Unzip\Exception\CouldNotReadFileException
     */
    public function testCanUnzip(): void 
    {
        $fileType = new FileType(self::$helper->getZIPFile());
        $contents = $fileType->getContents();

        self::assertCount(
            2,
            $contents
        );
        
        $keys = array_keys($contents);
        $values = array_values($contents);

        self::assertSame(
            'file1.txt',
            $keys[0]
        );

        self::assertSame(
            'Hello World 1!',
            $values[0]
        );
        
        self::assertSame(
            'file2.txt',
            $keys[1]
        );

        self::assertSame(
            'Hello World 2!',
            $values[1]
        );
    }
    
    public function testThrowsException(): void 
    {
        $this->expectException(CouldNotReadFileException::class);
        $fileType = new FileType('missing.zip');
        unset($fileType);
    }

    /**
     * @throws \BitAndBlack\Unzip\Exception\CouldNotReadFileException
     */
    public function testCanExtract(): void 
    {
        $fileType = new FileType(self::$helper->getZIPFile());
        $success = $fileType->extractTo(self::$helper->getFolderName());
        
        self::assertTrue($success);
        self::assertFileExists(self::$helper->getFolderName());
    }
}
