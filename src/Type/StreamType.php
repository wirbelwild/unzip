<?php

/**
 * Bit&Black Unzip. Unpacks ZIP files on the fly.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Unzip\Type;

use PhpZip\Exception\ZipException;
use PhpZip\ZipFile;

/**
 * Class StreamType.
 * 
 * @package BitAndBlack\Unzip
 */
class StreamType implements TypeInterface
{
    /**
     * @var array<string, string>
     */
    private array $contents;

    /**
     * @var \PhpZip\ZipFile 
     */
    private ZipFile $zipFile;

    /**
     * StreamType constructor.
     * 
     * @param resource $zipStream
     * @throws \PhpZip\Exception\ZipException
     */
    public function __construct($zipStream)
    {
        $this->zipFile = new ZipFile();
        $this->zipFile->openFromStream($zipStream);

        foreach ($this->zipFile->getEntries() as $entry) {
            $content = '';
            
            if (null !== $data = $entry->getData()) {
                $content = $data->getDataAsString();
            }
            
            $this->contents[$entry->getName()] = $content;
        }
    }

    /**
     * Returns a list of all files and their content.
     *
     * @return array<string, string>
     */
    public function getContents(): array
    {
        return $this->contents;
    }

    /**
     * Extracts the ZIP content to the file system.
     *
     * @param string $destination              The destination folder name.
     * @param array<string>|string|null $files The entries to extract. It accepts either a single entry name or an array of names.
     *                                         If null, all files will be extracted.
     * @return bool
     */
    public function extractTo(string $destination, $files = null): bool
    {
        if (!file_exists($destination) && !mkdir($destination) && !is_dir($destination)) {
            return false;
        }

        try {
            $this->zipFile->extractTo($destination, $files);
        } catch (ZipException $exception) {
            return false;
        }
        
        return true;
    }
}