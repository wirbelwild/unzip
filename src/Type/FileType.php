<?php

/**
 * Bit&Black Unzip. Unpacks ZIP files on the fly.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Unzip\Type;

use BitAndBlack\Unzip\Exception\CouldNotReadFileException;
use PhpZip\Exception\ZipException;
use PhpZip\ZipFile;

/**
 * The FileType class holds and unpacks a ZIP file.
 * 
 * @package BitAndBlack\Unzip
 */
class FileType implements TypeInterface
{
    /**
     * @var string
     */
    protected string $file;

    /**
     * @var array<string, string>
     */
    protected array $contents;
    
    /**
     * @var \PhpZip\ZipFile
     */
    private ZipFile $zipFile;

    /**
     * FileType constructor.
     *
     * @param string $file Path to the file.
     * @throws \BitAndBlack\Unzip\Exception\CouldNotReadFileException
     */
    public function __construct(string $file)
    {
        $this->zipFile = new ZipFile();
        
        try {
            $this->zipFile->openFile($file);
        } catch (ZipException $exception) {
            throw new CouldNotReadFileException($file);
        }

        foreach ($this->zipFile->getEntries() as $entry) {
            $content = '';

            if (null !== $data = $entry->getData()) {
                $content = $data->getDataAsString();
            }

            $this->contents[$entry->getName()] = $content;
        }
    }

    /**
     * Returns the path to the file.
     * 
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * Returns a list of all files and their content.
     * 
     * @return array<string, string>
     */
    public function getContents(): array
    {
        return $this->contents;
    }

    /**
     * Extracts the ZIP content to the file system.
     * 
     * @param string $destination              The destination folder name. 
     * @param array<string>|string|null $files The entries to extract. It accepts either a single entry name or an array of names.
     *                                         If null, all files will be extracted.
     * @return bool
     */
    public function extractTo(string $destination, $files = null): bool 
    {
        if (!file_exists($destination) && !mkdir($destination) && !is_dir($destination)) {
            return false;
        }

        try {
            $this->zipFile->extractTo($destination, $files);
        } catch (ZipException $exception) {
            return false;
        }

        return true;
    }
}