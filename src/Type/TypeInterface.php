<?php

/**
 * Bit&Black Unzip. Unpacks ZIP files on the fly.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Unzip\Type;

/**
 * Interface TypeInterface.
 * 
 * @package BitAndBlack\Unzip
 */
interface TypeInterface
{
    /**
     * Returns a list of all files and their content.
     *
     * @return array<string, string>
     */
    public function getContents(): array;

    /**
     * Extracts the ZIP file to the file system.
     *
     * @param string $destination              The destination folder name.
     * @param array<string>|string|null $files The entries to extract. It accepts either a single entry name or an array of names.
     *                                         If null, all files will be extracted.
     * @return bool
     */
    public function extractTo(string $destination, $files = null): bool;
}