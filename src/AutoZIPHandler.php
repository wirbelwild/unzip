<?php

/**
 * Bit&Black Unzip. Unpacks ZIP files on the fly.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Unzip;

use BitAndBlack\Unzip\Exception\CouldNotHandleFileTypeException;
use BitAndBlack\Unzip\Type\FileType;
use BitAndBlack\Unzip\Type\TypeInterface;
use BitAndBlack\Unzip\Type\StreamType;
use BitAndBlack\Unzip\Type\StringType;

/**
 * Class AutoZIP.
 * 
 * @package BitAndBlack\Unzip
 */
class AutoZIPHandler
{
    /**
     * @param mixed $input
     * @return \BitAndBlack\Unzip\Type\TypeInterface
     * @throws \BitAndBlack\Unzip\Exception\CouldNotHandleFileTypeException
     * @throws \BitAndBlack\Unzip\Exception\CouldNotReadFileException
     * @throws \PhpZip\Exception\ZipException
     */
    public static function create($input): TypeInterface
    {
        if (is_string($input) && file_exists($input)) {
            return new FileType($input);
        }
        
        if (is_resource($input)) {
            return new StreamType($input);
        }
        
        if (is_string($input)) {
            return new StringType($input);
        }
        
        throw new CouldNotHandleFileTypeException(
            gettype($input)
        );
    }
}