<?php

/**
 * Bit&Black Unzip. Unpacks ZIP files on the fly.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Unzip;

use Throwable;

/**
 * Class Exception.
 * 
 * @package BitAndBlack\Unzip
 */
class Exception extends \Exception
{
    /**
     * Exception constructor.
     * 
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}