<?php

/**
 * Bit&Black Unzip. Unpacks ZIP files on the fly.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Unzip\Exception;

use BitAndBlack\Unzip\Exception;

/**
 * Class CouldNotHandleFileTypeException.
 * 
 * @package BitAndBlack\Unzip\Exception
 */
class CouldNotHandleFileTypeException extends Exception
{
    /**
     * CouldNotHandleFileTypeException constructor.
     * 
     * @param string $fileType
     */
    public function __construct(string $fileType)
    {
        parent::__construct('Cannot handle file type "'.$fileType.'".');
    }
}