<?php

/**
 * Bit&Black Unzip. Unpacks ZIP files on the fly.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Unzip\Exception;

use BitAndBlack\Unzip\Exception;

/**
 * Class CouldNotReadFileException.
 * 
 * @package BitAndBlack\Unzip\Exception
 */
class CouldNotReadFileException extends Exception
{
    /**
     * CouldNotReadFileException constructor.
     * 
     * @param string $file
     */
    public function __construct(string $file)
    {
        parent::__construct('Could not open file "'.$file.'".');
    }
}